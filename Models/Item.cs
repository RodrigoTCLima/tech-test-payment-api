using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Item
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nome  { get; set; }
        [Required]
        public string Descricao  { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "A quantidade não pode ser negativa.")]
        public int Quantidade { get; set; } = 0;
    }
}
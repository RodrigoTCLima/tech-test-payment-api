using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int IdVendedor  { get; set; }
        [ForeignKey("IdVendedor")]
        public Vendedor Vendedor { get; set; }
        [Required]
        public DateTime DataVenda  { get; set; }
        [Required]
        public EnumStatusVenda Status { get; set; } = EnumStatusVenda.AguardandoPagamento;
        [InverseProperty("Venda")]
        public List<ItemVendido> ItensVendidos { get; set; } = new List<ItemVendido>();

    }
}
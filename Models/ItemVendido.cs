using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class ItemVendido
    {
        public int IdVenda { get; set; }
        [ForeignKey("IdVenda")]
        public Venda Venda { get; set; }
        public int IdItem { get; set; }
        [ForeignKey("IdItem")]
        public Item Item { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "A quantidade não pode ser negativa.")]
        public int Quantidade { get; set; } = 1;
    }
}
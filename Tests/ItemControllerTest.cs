using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Controllers;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;
using Xunit;

namespace tech_test_payment_api.Tests
{
    public class ItemControllerTests
    {
        private readonly LojaContext _context;
        private readonly ItemController _controller;

        public ItemControllerTests()
        {
            var options = new DbContextOptionsBuilder<LojaContext>()
                .UseInMemoryDatabase(databaseName: "LojaContextInMemory")
                .Options;

            _context = new LojaContext(options);
            _controller = new ItemController(_context);

            // Limpar o contexto antes de cada teste
            _context.Vendedores.RemoveRange(_context.Vendedores);
            _context.Vendas.RemoveRange(_context.Vendas);
            _context.Itens.RemoveRange(_context.Itens);
            _context.ItensVendidos.RemoveRange(_context.ItensVendidos);
            _context.SaveChanges();
        }

        [Fact]
        public void RegistrarItem_RetornaOk_QuandoDadosValidos()
        {
            // Arrange
            var item = new Item
            {
                Nome = "item1",
                Descricao = "Exemplo de item",
                Quantidade = 1
            };

            // Act
            var result = _controller.RegistrarItem(item);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<Item>(okResult.Value);
            Assert.Equal(item.Nome, returnValue.Nome);
            Assert.Equal(1, _context.Itens.Count());
        }

        [Fact]
        public void RegistrarItem_RetornaBadRequest_QuandoDadosInvalidos()
        {
            // Arrange
            var item = new Item
            {
                Nome = "item1",
                Descricao = "exemplo para erro",
                Quantidade = -1
            };

            // Act
            var result = _controller.RegistrarItem(item);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("A quantidade não pode ser negativa.", ((dynamic)badRequestResult.Value).Erro);
        }

        [Fact]
        public void BuscarItemPorId_RetornaOk_QuandoItemExiste()
        {
            // Arrange
            var item = new Item
            {
                Id = 1,
                Nome = "item",
                Descricao = "É um item",
                Quantidade = 4
            };
            _context.Itens.Add(item);
            _context.SaveChanges();

            // Act
            var result = _controller.BuscarItemPorId(1);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<Item>(okResult.Value);
            Assert.Equal(item.Id, returnValue.Id);
        }

        [Fact]
        public void BuscarItemPorId_RetornaNotFound_QuandoItemNaoExiste()
        {
            // Arrange
            _context.Itens.RemoveRange(_context.Itens);
            _context.SaveChanges();

            // Act
            var result = _controller.BuscarItemPorId(1);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void BuscarItensPorNome_RetornaOk_QuandoItemExiste()
        {
            // Arrange
            var NomeProcurado = "NomeProcurado";
            var item = new Item
            {
                Nome = NomeProcurado,
                Descricao = "Descricao",
                Quantidade = 1
            };
            _context.Itens.Add(item);
            _context.SaveChanges();

            // Act
            var result = _controller.BuscarItensPorNome(NomeProcurado);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<List<Item>>(okResult.Value);
            Assert.Single(returnValue);
            Assert.Equal(NomeProcurado, returnValue[0].Nome);
        }

        [Fact]
        public void BuscarItensPorNome_RetortnaNotFound_QuandoNenhumItemComONomeExiste()
        {
            // Arrange
            _context.Itens.RemoveRange(_context.Itens);
            _context.SaveChanges();

            // Act
            var result = _controller.BuscarItensPorNome("Nome");

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void BuscarItensPorDescricao_RetornaOk_QuandoItemExiste()
        {
            // Arrange
            var DescricaoProcurada = "DescricaoProcurada";
            var item = new Item
            {
                Nome = "Nome",
                Descricao = DescricaoProcurada,
                Quantidade = 1
            };
            _context.Itens.Add(item);
            _context.SaveChanges();

            // Act
            var result = _controller.BuscarItensPorDescricao(DescricaoProcurada);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<List<Item>>(okResult.Value);
            Assert.Single(returnValue);
            Assert.Equal(DescricaoProcurada, returnValue[0].Descricao);
        }

        [Fact]
        public void BuscarItensPorDescricao_RetortnaNotFound_QuandoNenhumItemComADescricaoExiste()
        {
            // Arrange
            _context.Itens.RemoveRange(_context.Itens);
            _context.SaveChanges();

            // Act
            var result = _controller.BuscarItensPorDescricao("Descricao");

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void BuscarItensPorQuantidade_RetornaOk_QuandoItemExiste()
        {
            // Arrange
            var QuantidadeProcurada = 10;
            var item = new Item
            {
                Nome = "Nome",
                Descricao = "Descricao",
                Quantidade = QuantidadeProcurada
            };
            _context.Itens.Add(item);
            _context.SaveChanges();

            // Act
            var result = _controller.BuscarItensPorQuantidade(QuantidadeProcurada);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<List<Item>>(okResult.Value);
            Assert.Single(returnValue);
            Assert.Equal(QuantidadeProcurada, returnValue[0].Quantidade);
        }

        [Fact]
        public void BuscarItensPorQuantidade_RetortnaNotFound_QuandoNenhumItemComPeloMenosAQuantidadeExiste()
        {
            // Arrange
            _context.Itens.RemoveRange(_context.Itens);
            _context.SaveChanges();

            // Act
            var result = _controller.BuscarItensPorQuantidade(1);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void ModificaItem_RetornaOk_QuandoItemExisteEModifica()
        {
            // Arrange
            var itemId = 1;
            var item = new Item
            {
                Id = itemId,
                Nome = "Item",
                Descricao = "Descricao",
                Quantidade = 1
            };
            _context.Itens.Add(item);
            _context.SaveChanges();

            var itemAtualizado = new ItemUpdateDTO
            {
                Nome = "ItemModificado",
                Descricao = "DescricaoModiicada",
                Quantidade = 10
            };
            // Act
            var result = _controller.ModificarItem(itemId, itemAtualizado);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<Item>(okResult.Value);
            Assert.Equal(itemId, returnValue.Id);
            Assert.Equal(itemAtualizado.Nome, returnValue.Nome);
            Assert.Equal(itemAtualizado.Descricao, returnValue.Descricao);
            Assert.Equal(itemAtualizado.Quantidade, returnValue.Quantidade);
        }

        [Fact]
        public void ModificaItem_RetornaNotFound_QuandoItemNaoExiste()
        {
            // Arrange
            _context.Itens.RemoveRange(_context.Itens);
            _context.SaveChanges();

            var itemId = 1;
            var itemAtualizado = new ItemUpdateDTO { };

            // Act
            var result = _controller.ModificarItem(itemId, itemAtualizado);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void ModificaItem_RetornaBadRequest_QuandoDadosInvalidos()
        {
            // Arrange
            var itemId = 1;
            var item = new Item
            {
                Id = itemId,
                Nome = "Item",
                Descricao = "Descricao",
                Quantidade = 1
            };
            _context.Itens.Add(item);
            _context.SaveChanges();

            var itemAtualizado = new ItemUpdateDTO
            {
                Quantidade = -1
            };
            // Act
            var result = _controller.ModificarItem(itemId, itemAtualizado);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

    }
}
using Xunit;
using tech_test_payment_api.Context;
using tech_test_payment_api.Controllers;
using tech_test_payment_api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

namespace tech_test_payment_api.Tests
{
    public class VendaControllerTests
    {
        private readonly LojaContext _context;
        private readonly VendaController _controller;

        public VendaControllerTests()
        {
            var options = new DbContextOptionsBuilder<LojaContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            _context = new LojaContext(options);
            _controller = new VendaController(_context);

            // Limpar o contexto antes de cada teste
            _context.Vendedores.RemoveRange(_context.Vendedores);
            _context.Vendas.RemoveRange(_context.Vendas);
            _context.Itens.RemoveRange(_context.Itens);
            _context.ItensVendidos.RemoveRange(_context.ItensVendidos);
            _context.SaveChanges();
        }

        [Fact]
        public void RegistrarVenda_RetornaOk_QuandoDadosCorretos()
        {
            // Arrange
            var vendedor = new Vendedor
            {
                Id = 1,
                CPF = "12345678910",
                Nome = "Vendedor",
                Telefone = "123345456",
                Email = "email@email.com"
            };
            var item = new Item
            {
                Id = 1,
                Nome = "Item1",
                Descricao = "Este é o item 1",
                Quantidade = 1000
            };
            _context.Vendedores.Add(vendedor);
            _context.Itens.Add(item);
            _context.SaveChanges();

            var itensVendidos = new List<ItemVendido>
            {
                new ItemVendido
                {
                    IdItem = 1,
                    Quantidade = 10
                }
            };
            var venda = new Venda
            {
                IdVendedor = vendedor.Id,
                ItensVendidos = itensVendidos
            };

            // Act
            var result = _controller.RegistrarVenda(venda);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<Venda>(okResult.Value);
            Assert.Equal(venda.IdVendedor, returnValue.IdVendedor);
            Assert.Equal(item.Quantidade, _context.Itens.Find(1).Quantidade);
        }

        [Fact]
        public void RegistrarVenda_RetornaBadRequest_QuandoIdVendedorInvalido()
        {
            // Arrange
            var item = new Item
            {
                Id = 1,
                Nome = "Item1",
                Descricao = "Este é o item 1",
                Quantidade = 1000
            };
            _context.Itens.Add(item);
            _context.SaveChanges();

            var itensVendidos = new List<ItemVendido>
            {
                new ItemVendido
                {
                    IdItem = 1,
                    Quantidade = 10
                }
            };
            var venda = new Venda
            {
                IdVendedor = 1,
                ItensVendidos = itensVendidos
            };

            // Act
            var result = _controller.RegistrarVenda(venda);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Precisa-se de um Id válido de vendedor para realizar a venda.", ((dynamic)badRequestResult.Value).Erro);
        }
        
        [Fact]
        public void RegistrarVenda_RetornaBadRequest_QuandoItemInvalido()
        {
            // Arrange
            var vendedor = new Vendedor
            {
                Id = 1,
                CPF = "12345678910",
                Nome = "Vendedor",
                Telefone = "123345456",
                Email = "email@email.com"
            };
            var item = new Item
            {
                Id = 1,
                Nome = "Item1",
                Descricao = "Este é o item 1",
                Quantidade = 1000
            };
            _context.Vendedores.Add(vendedor);
            _context.Itens.Add(item);
            _context.SaveChanges();

            var itensVendidos = new List<ItemVendido>
            {
                new ItemVendido
                {
                    IdItem = 1,
                    Quantidade = 10000
                }
            };
            var venda = new Venda
            {
                IdVendedor = vendedor.Id,
                ItensVendidos = itensVendidos
            };

            // Act
            var result = _controller.RegistrarVenda(venda);

            // Assert
 var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Item inválido ou quantidade vendida excede a quantidade em estoque", ((dynamic)badRequestResult.Value).Erro);
        }

    }
}
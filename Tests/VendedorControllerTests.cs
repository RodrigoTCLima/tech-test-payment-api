using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Controllers;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;
using Xunit;

namespace tech_test_payment_api.Tests
{
    public class VendedorControllerTests
    {
        private readonly LojaContext _context;
        private readonly VendedorController _controller;

        public VendedorControllerTests()
        {
            var options = new DbContextOptionsBuilder<LojaContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            _context = new LojaContext(options);
            _controller = new VendedorController(_context);

            // Limpar o contexto antes de cada teste
            _context.Vendedores.RemoveRange(_context.Vendedores);
            _context.Vendas.RemoveRange(_context.Vendas);
            _context.Itens.RemoveRange(_context.Itens);
            _context.ItensVendidos.RemoveRange(_context.ItensVendidos);
            _context.SaveChanges();
        }

        [Fact]
        public void RegistrarVendedor_RetornaOk_QuandoDadosValidos()
        {
            // Arrange
            var vendedor = new Vendedor { CPF = "12345678901", Nome = "João", Email = "joao@example.com", Telefone = "123456789" };

            // Act
            var result = _controller.RegistrarVendedor(vendedor);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<Vendedor>(okResult.Value);
            Assert.Equal(vendedor.CPF, returnValue.CPF);
            Assert.Equal(1, _context.Vendedores.Count());
        }

        [Fact]
        public void RegistrarVendedor_RetornaBadRequest_QuandoDadosInvalidos()
        {
            // Arrange
            var vendedor = new Vendedor { CPF = null, Nome = null };

            // Act
            var result = _controller.RegistrarVendedor(vendedor);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Nem o nome, nem o cpf podem estar vazios", ((dynamic)badRequestResult.Value).Erro);
        }

        [Fact]
        public void BuscarVendedorPorId_RetornaOk_QuandoVendedorExiste()
        {
            // Arrange
            var vendedor = new Vendedor
            {
                Id = 1,
                Nome = "João",
                CPF = "12345678901",
                Email = "joao@example.com",
                Telefone = "123456789"
            };
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();

            // Act
            var result = _controller.BuscarVendedorPorId(1);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<Vendedor>(okResult.Value);
            Assert.Equal(vendedor.Id, returnValue.Id);
        }

        [Fact]
        public void BuscarVendedorPorId_RetornaNotFound_QuandoVendedorNaoExiste()
        {
            // Arrange
            _context.Vendedores.RemoveRange(_context.Vendedores); // Certifica-se de que o banco está vazio
            _context.SaveChanges();

            // Act
            var result = _controller.BuscarVendedorPorId(1);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }
    }
}

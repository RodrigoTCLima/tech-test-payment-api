using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class LojaContext : DbContext
    {
        public LojaContext(DbContextOptions<LojaContext> options) : base(options) {}
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Item> Itens { get; set; }
        public DbSet<ItemVendido> ItensVendidos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ItemVendido>()
            .HasKey(iv => new { iv.IdVenda, iv.IdItem });
 
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Vendedor>().HasData(
                new Vendedor { Id = 1, Nome = "João", CPF = "12345678901", Email = "joao@exemplo.com", Telefone = "12345678" },
                new Vendedor { Id = 2, Nome = "Maria", CPF = "98765432100", Email = "maria@exemplo.com", Telefone = "12387654" }
            );

            modelBuilder.Entity<Item>().HasData(
                new Item { Id = 1, Nome = "Bola", Quantidade = 50, Descricao = "é uma bola" },
                new Item { Id = 2, Nome = "Boneca", Quantidade = 30, Descricao = "é uma boneca" }
            );
}
    }
}
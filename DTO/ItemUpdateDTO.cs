public class ItemUpdateDTO
{
    public string Nome { get; set; }
    public string Descricao { get; set; }
    public int? Quantidade { get; set; }
}

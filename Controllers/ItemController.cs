using System.Reflection.Metadata.Ecma335;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Protocol;
using Microsoft.IdentityModel.Tokens;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ItemController : ControllerBase
    {
        private readonly LojaContext _context;
        public ItemController(LojaContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var itens = _context.Itens.ToList();
            return Ok(itens);
        }

        [HttpPost]
        public IActionResult RegistrarItem(Item item)
        {

            if (item.Quantidade < 0)
                return BadRequest(new { Erro = "A quantidade não pode ser negativa." });
            Item NewItem = new()
            {
                Nome = item.Nome,
                Descricao = item.Descricao,
                Quantidade = item.Quantidade
            };

            _context.Itens.Add(NewItem);
            _context.SaveChanges();
            return Ok(NewItem);
        }

        [HttpGet("id/{id}")]
        public IActionResult BuscarItemPorId(int id)
        {
            var item = _context.Itens.Find(id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        [HttpGet("nome/{nome}")]
        public IActionResult BuscarItensPorNome(string nome){
            var item = _context.Itens.Where(x => x.Nome.Contains(nome)).ToList();
            if (item.IsNullOrEmpty()) return NotFound();
            return Ok(item);
        }

        [HttpGet("descricao/{descricao}")]
        public IActionResult BuscarItensPorDescricao(string descricao)
        {
            var item = _context.Itens.Where(x => x.Descricao.Contains(descricao)).ToList();
            if (item.IsNullOrEmpty()) return NotFound();
            return Ok(item);
        }

        [HttpGet("quantidade/{quantidade}")]
        public IActionResult BuscarItensPorQuantidade(int quantidade)
        {
            var item = _context.Itens.Where(x => x.Quantidade >= quantidade).ToList();
            if (item.IsNullOrEmpty()) return NotFound();
            return Ok(item);
        }

        [HttpPut("{id}")]
        public IActionResult ModificarItem(int id, ItemUpdateDTO item)
        {
            var itemAtual = _context.Itens.Find(id);
            
            if (itemAtual == null) return NotFound();
            
            if(!string.IsNullOrWhiteSpace(item.Nome))
            {
                itemAtual.Nome = item.Nome.Trim();
            }
            if(!string.IsNullOrWhiteSpace(item.Descricao))
            {
                itemAtual.Descricao = item.Descricao.Trim();
            }
            if(item.Quantidade.HasValue)
            {
                if (item.Quantidade.Value <= 0) return BadRequest(new { Erro = "Não é permitido um valor menor do que 0" });
                itemAtual.Quantidade = item.Quantidade.Value;
            }
            _context.Itens.Update(itemAtual);
            _context.SaveChanges();
            return Ok(itemAtual);
        }
    }
}
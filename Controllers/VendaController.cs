using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase 
    {
        private readonly LojaContext _context;
        public VendaController(LojaContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda1)
        {
            var vendedor = _context.Vendedores.Find(venda1.IdVendedor);
            if( vendedor == null)
            {
                return BadRequest(new { Erro = "Precisa-se de um Id válido de vendedor para realizar a venda." });
            }

            Venda venda = new()
            {
                DataVenda = DateTime.Now,
                IdVendedor = venda1.IdVendedor,
                Status = EnumStatusVenda.AguardandoPagamento
            };
            _context.Vendas.Add(venda);

            List<ItemVendido> ItensVendidosParaSalvar = new();
            List<Item> ItensParaSalvar = new();

            if (venda1.ItensVendidos == null) return BadRequest(new { Erro = "Não há itens vendidos nessa venda" });
            foreach (var itemVendido in venda1.ItensVendidos)
            {
                Item itemConsultado = _context.Itens.Find(itemVendido.IdItem);
                if (itemConsultado == null || itemConsultado.Quantidade < itemVendido.Quantidade)
                    return BadRequest(new { Erro = "Item inválido ou quantidade vendida excede a quantidade em estoque" });

                itemConsultado.Quantidade -= itemVendido.Quantidade;
                ItensParaSalvar.Add(itemConsultado);
                ItensVendidosParaSalvar.Add(itemVendido);
            }
            _context.SaveChanges();

            foreach (Item itemModificado in ItensParaSalvar) _context.Itens.Update(itemModificado);
            foreach (ItemVendido itemVendido1 in ItensVendidosParaSalvar)
            {
                itemVendido1.IdVenda = venda.Id;
                _context.ItensVendidos.Add(itemVendido1);
            }
                
            _context.SaveChanges();
            return Ok(venda);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var vendas = _context.Vendas.ToList();
            return Ok(vendas);
        }

        [HttpGet("{id}")]
        public IActionResult ObterVendaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if(venda == null) return NotFound();
            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarStatusVenda(int id, EnumStatusVenda statusVenda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if(vendaBanco == null) return NotFound();
            else if(vendaBanco.Status == EnumStatusVenda.AguardandoPagamento && statusVenda == EnumStatusVenda.Cancelada || statusVenda == EnumStatusVenda.PagamentoAprovado)
            {
                vendaBanco.Status = statusVenda;
                _context.Vendas.Update(vendaBanco);
                _context.SaveChanges();
                return Ok(vendaBanco);
            }
            else if(vendaBanco.Status == EnumStatusVenda.PagamentoAprovado && statusVenda == EnumStatusVenda.Cancelada || statusVenda == EnumStatusVenda.EnviadoParaTransportadora)
            {
                vendaBanco.Status = statusVenda;
                _context.Vendas.Update(vendaBanco);
                _context.SaveChanges();
                return Ok(vendaBanco);
            }
            else if(vendaBanco.Status == EnumStatusVenda.EnviadoParaTransportadora && statusVenda == EnumStatusVenda.Entregue)
            {
                vendaBanco.Status = statusVenda;
                _context.Vendas.Update(vendaBanco);
                _context.SaveChanges();
                return Ok(vendaBanco);
            }
            else
            {
                return BadRequest(new { Erro = "Novo status inválido" });
            }
        }
    }
}
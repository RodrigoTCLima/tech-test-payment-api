using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly LojaContext _context;

        public VendedorController(LojaContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult RegistrarVendedor(Vendedor vendedor)
        {
            if(vendedor.CPF == null || vendedor.Nome == null)
            {
                return BadRequest(new { Erro = "Nem o nome, nem o cpf podem estar vazios" });
            }
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var vendedores = _context.Vendedores.ToList();
            return Ok(vendedores);
        }

        [HttpGet("id/{id}")]
        public IActionResult BuscarVendedorPorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            if(vendedor == null) return NotFound();
            return Ok(vendedor);
        }

        [HttpGet("nome/{nome}")]
        public IActionResult BuscarVendedorPorNome(string nome)
        {
            var vendedor = _context.Vendedores.Where(x => x.Nome.Contains(nome)).ToList();
            return Ok(vendedor);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarVendedor(int id, VendedorUpdateDTO vendedor)
        {
            var vendedorBanco = _context.Vendedores.Find(id);
            if (vendedorBanco == null) return NotFound();

            if(!string.IsNullOrWhiteSpace(vendedor.Nome))
                vendedorBanco.Nome = vendedor.Nome.Trim();
            if(!string.IsNullOrWhiteSpace(vendedor.Telefone))
                vendedorBanco.Telefone = vendedor.Telefone.Trim();
            if(!string.IsNullOrWhiteSpace(vendedor.Email))
                vendedorBanco.Email = vendedor.Email.Trim();
            if (!string.IsNullOrWhiteSpace(vendedor.CPF))
                vendedorBanco.CPF = vendedor.CPF.Trim();

            _context.Vendedores.Update(vendedorBanco);
            _context.SaveChanges();

            return Ok(vendedorBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult DeletarVendedor(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);
            if (vendedorBanco == null) return NotFound();

            _context.Vendedores.Remove(vendedorBanco);
            _context.SaveChanges();
            return NoContent();
        }
    }
}
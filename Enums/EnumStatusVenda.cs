namespace tech_test_payment_api.Enums
{
    public enum EnumStatusVenda
    {
        Cancelada = 0,
        AguardandoPagamento = 1,
        PagamentoAprovado = 2,
        EnviadoParaTransportadora = 3,
        Entregue = 4
    }
}